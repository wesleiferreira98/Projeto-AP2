import java.io.Serializable;
import java.util.ArrayList;
class Usuario implements Serializable{
    private int ID;
    private String Nome;
    private long CPF;
    public Usuario(int id, String nome, long cpf){
        this.ID = id;
        this.Nome=nome;
        this.CPF= cpf;
    }
    public Usuario(){}

    public  void SetID(int id){
        this.ID= id;
    }

    public void SetNome(String nome){
        this.Nome=nome;
    }

    public void SetCPF( long CPF){
        this.CFP=CPF;
    }

    public int GetID(){
        return ID;
    }

    public String GetNome(){
        return Nome;
    }

    public long GetCPF(){
        return CPF;
    }
    @Override
    public String toString(){
        return "ID "+GetID()+"\n"+"Nome "+GetNome()+"\n"+"CFP "+GetCPF()+"\n";
    }



}