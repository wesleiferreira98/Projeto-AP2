import java.io.Serializable;
import java.sql.Date;
class Vacina implements Serializable{
    private String Fabricate;
    private long lote;
    private Date DataFabricacao;
    private Date DataVencimento;
    public Vacina(String fab,long lot,Date DF,Date DV){
        this.Fabricate=fab;
        this.lote=lot;
        this.DataFabricacao= DF;
        this.DataVencimento = DV;

    }
    public Vacina(){}
    public void  SetFabricante(String fab){
        this.Fabricate=fab;

    }
    public void SetLote(long lot){
        this.lote=lot;
    }
    public void SetDataFabricacao(Date DF){
        this.DataFabricacao=DF;
    }
    public void SetDataVencimento(Date DV){
        this.DataVencimento=DV;
    }
    public String GetFabricante(){
        return Fabricate;
    }
    public long GetLote(){
        return lote;
    }
    public Date GetDataFabricacao(){
        return DataFabricacao;
    }
    public Date GetDataVencimento(){
        return DataVencimento;
    }
    @Override
    public String toString(){
        return "Fabricante da Vacina "+GetFabricante()+"\n"+"Lote "+GetLote()+"\n"+" Data de Fabricação "+String.valueOf(GetDataFabricacao())+"\n"+"Data de vencimento "+String.valueOf(GetDataVencimento())+"\n\n";
    }
}