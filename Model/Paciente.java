import java.io.Serializable;
class Paciente implements Serializable{
    private String Nome;
    private long Cep;
    private long CPF;
    private long Telefone;
    private Aplicador aplicador;
    private Vacina vacina;
    public Paciente( String nome,long id, long cpf,long tel,Aplicador apli,Vacina vac){
        this.Nome=nome;
        this.Cep=id;
        this.CPF= cpf;
        this.Telefone = tel;
        this.aplicador=apli;
        this.vacina=vac;
    }
    public Paciente(){}

    public  void SetCep(long id){
        this.Cep= id;
    }

    public void SetNome(String nome){
        this.Nome=nome;
    }
    public void SetVacina(Vacina v){
        this.vacina=v;
    }
    public void SetCPF( long CPF){
        this.CFP=CPF;
    }

    public void SetTelefone(long fun){
        this.Telefone=fun;
    }

    public String GetNome(){
        return Nome;
    }

    public long GetCPF(){
        return CPF;
    }
    public long GetCep(){
        return Cep;
    }
    
    public long GetFone(){
        return Telefone;
    }
    public Vacina GetVacina(){
        return vacina;
    }
    public Aplicador GetAplicador(){
        return aplicador;
    }
    @Override
    public String toString(){
        return "Nome "+GetNome()+"\n"+"CFP "+GetCPF()+"\n"+"Cep "+GetCep()+"\n"+"Telefone "+GetFone()+"\n"+"Aplicador "+GetAplicador().GetNome()+"\n"+"Vacina "+GetVacina().GetFabricante()+"\n\n";
    }

}